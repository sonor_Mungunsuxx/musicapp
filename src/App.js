import {useState, useEffect} from 'react';
import Player from './components/Player/Player.js';

function App() {
  const [songs] = useState([
    {
      title: "We Don't talk no more",
      artist: "Charie Puth",
      img_src: "./images/1.png",
      src: "./music/Charie Puth Ft. Selena Gomez - We don't talk anymore.mp3"
    },
    {
      title: "To the Girl With Red Converse",
      artist: "peepoo",
      img_src: "./images/8.png",
      src: "./music/To the Girl With Red Converse - Lofi.mp3"
    },
    {
      title: "Japanese Lofi Album",
      artist: "Unknown",
      img_src: "./images/12.png",
      src: "./music/the bootleg boy - ☯ Japanese Lofi HipHop Mix.mp3"
    },
    {
      title: "Song 4",
      artist: "Artist 4",
      img_src: "./images/song-4.jpg",
      src: "./music/somebody-new.mp3"
    }
  ]);

  const [currentSongIndex, setCurrentSongIndex] = useState(0);
  const [nextSongIndex, setNextSongIndex] = useState(0);

  useEffect(() => {
    setNextSongIndex(() => {
      if (currentSongIndex + 1 > songs.length - 1) {
        return 0;
      } else {
        return currentSongIndex + 1;
      }
    });
  }, [currentSongIndex]);

  return (
    <div className="App">
      <div className="Login">

      </div>
      <Player 
        currentSongIndex={currentSongIndex} 
        setCurrentSongIndex={setCurrentSongIndex} 
        nextSongIndex={nextSongIndex} 
        songs={songs}
      />
    </div>
  );
}

export default App;
